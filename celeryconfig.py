broker_url = 'redis://'
result_backend = 'redis://'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Asia/Ho_Chi_Minh'
enable_utc = True
